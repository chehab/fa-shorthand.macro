const fs = require('fs')
const path = require('path')
const pluginTester = require('babel-plugin-tester').default
const plugin = require('babel-plugin-macros')


const fixturesDir = path.join(__dirname, '__fixtures__')


pluginTester({
  plugin,
  snapshot: true,
  fixtures: fixturesDir,
  pluginName: 'fa-shorthand.macro',
  babelOptions: require('./babel.config.js'),
})
