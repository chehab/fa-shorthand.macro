import React from 'react'
import {
  Icon,
  IconsStack,
} from 'fa-shorthand.macro'

function App() {
  const checkIcon = Icon.regular.check
  return (
    <div className="app-root">
      <IconsStack>
        <Icon icon={Icon.regular.square} color="green"/>
        <Icon icon={checkIcon} inverse transform="shrink-6"/>
      </IconsStack>

      <IconsStack size={3}>
        <Icon icon={Icon.regularFree.square} color="green"/>
        <Icon size="1x" icon={checkIcon} inverse transform="shrink-6"/>
      </IconsStack>

      <IconsStack>
        <Icon size={2} icon={Icon.regular.circle} color="green"/>
        <Icon size="3x" icon={Icon.brandFree.buyNLarge} inverse transform="shrink-6"/>
      </IconsStack>
    </div>
  )
}
