"use strict";

var _reactFontawesome = require("@fortawesome/react-fontawesome");

var _freeBrandSvgIcons = require("@fortawesome/free-brand-svg-icons");

var _freeRegularSvgIcons = require("@fortawesome/free-regular-svg-icons");

var _proRegularSvgIcons = require("@fortawesome/pro-regular-svg-icons");

var _react = _interopRequireDefault(require("react"));

function _interopRequireDefault(obj) {
  return obj && obj.__esModule ? obj : { default: obj };
}

function App() {
  var checkIcon = _proRegularSvgIcons.faCheck;
  return _react["default"].createElement(
    "div",
    {
      className: "app-root"
    },
    _react["default"].createElement(
      "span",
      {
        className: "fa-stack fa-1x"
      },
      _react["default"].createElement(_reactFontawesome.FontAwesomeIcon, {
        className: "fa-stack-1x",
        icon: _proRegularSvgIcons.faSquare,
        color: "green"
      }),
      _react["default"].createElement(_reactFontawesome.FontAwesomeIcon, {
        className: "fa-stack-1x",
        icon: checkIcon,
        inverse: true,
        transform: "shrink-6"
      })
    ),
    _react["default"].createElement(
      "span",
      {
        className: "fa-stack fa-3x"
      },
      _react["default"].createElement(_reactFontawesome.FontAwesomeIcon, {
        className: "fa-stack-3x",
        icon: _freeRegularSvgIcons.faSquare,
        color: "green"
      }),
      _react["default"].createElement(_reactFontawesome.FontAwesomeIcon, {
        className: "fa-stack-1x",
        icon: checkIcon,
        inverse: true,
        transform: "shrink-6"
      })
    ),
    _react["default"].createElement(
      "span",
      {
        className: "fa-stack fa-1x"
      },
      _react["default"].createElement(_reactFontawesome.FontAwesomeIcon, {
        className: "fa-stack-2x",
        icon: _proRegularSvgIcons.faCircle,
        color: "green"
      }),
      _react["default"].createElement(_reactFontawesome.FontAwesomeIcon, {
        className: "fa-stack-3x",
        icon: _freeBrandSvgIcons.faBuyNLarge,
        inverse: true,
        transform: "shrink-6"
      })
    )
  );
}
