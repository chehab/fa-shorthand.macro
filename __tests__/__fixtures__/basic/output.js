"use strict";

var _reactFontawesome = require("@fortawesome/react-fontawesome");

var _freeRegularSvgIcons = require("@fortawesome/free-regular-svg-icons");

var _freeSolidSvgIcons = require("@fortawesome/free-solid-svg-icons");

var _proSolidSvgIcons = require("@fortawesome/pro-solid-svg-icons");

var _react = _interopRequireDefault(require("react"));

function _interopRequireDefault(obj) {
  return obj && obj.__esModule ? obj : { default: obj };
}

function App() {
  return _react["default"].createElement(
    "div",
    {
      className: "app-root"
    },
    _react["default"].createElement(
      "h1",
      null,
      _react["default"].createElement(_reactFontawesome.FontAwesomeIcon, {
        icon: _proSolidSvgIcons.faCoffee
      }),
      "Coffee Bucks"
    ),
    _react["default"].createElement(
      "ul",
      null,
      _react["default"].createElement(
        "li",
        null,
        _react["default"].createElement(_reactFontawesome.FontAwesomeIcon, {
          icon: _freeSolidSvgIcons.faCoffee
        }),
        "Best Roaster for Morning cCoffee and all day long"
      )
    ),
    _react["default"].createElement(
      "h3",
      null,
      _react["default"].createElement(_reactFontawesome.FontAwesomeIcon, {
        spin: true,
        fixedWidth: true,
        icon: _freeRegularSvgIcons.faSpinner,
        size: "6x"
      }),
      "Roasting..."
    )
  );
}
