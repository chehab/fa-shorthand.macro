import React from 'react'
import { Icon } from 'fa-shorthand.macro'

function App() {

  return (
    <div className="app-root">
      <h1>
        <Icon icon={Icon.solid.coffee} />
        Coffee Bucks
      </h1>

      <ul>
        <li>
          <Icon icon={Icon.solidFree.coffee}/>
          Best Roaster for Morning cCoffee and all day long
        </li>
      </ul>

      <h3>
        <Icon spin fixedWidth icon={Icon.regularFree.spinner} size="6x"/>
        Roasting...
      </h3>
    </div>
  )
}
