"use strict";

var _reactFontawesome = require("@fortawesome/react-fontawesome");

var _freeBrandSvgIcons = require("@fortawesome/free-brand-svg-icons");

var _freeRegularSvgIcons = require("@fortawesome/free-regular-svg-icons");

var _proRegularSvgIcons = require("@fortawesome/pro-regular-svg-icons");

var _react = _interopRequireDefault(require("react"));

function _interopRequireDefault(obj) {
  return obj && obj.__esModule ? obj : { default: obj };
}

function App() {
  var checkIcon = _proRegularSvgIcons.faCheck;
  return _react["default"].createElement(
    "div",
    {
      className: "app-root"
    },
    _react["default"].createElement(
      "span",
      {
        className: "fa-layers fa-fw foobar-1"
      },
      _react["default"].createElement(_reactFontawesome.FontAwesomeIcon, {
        icon: _proRegularSvgIcons.faSquare,
        color: "green"
      }),
      _react["default"].createElement(_reactFontawesome.FontAwesomeIcon, {
        icon: checkIcon,
        inverse: true,
        transform: "shrink-6"
      })
    ),
    _react["default"].createElement(
      "span",
      {
        className: "fa-layers fa-fw"
      },
      _react["default"].createElement(_reactFontawesome.FontAwesomeIcon, {
        icon: _freeRegularSvgIcons.faSquare,
        color: "green"
      }),
      _react["default"].createElement(_reactFontawesome.FontAwesomeIcon, {
        size: "1x",
        icon: checkIcon,
        inverse: true,
        transform: "shrink-6"
      })
    ),
    _react["default"].createElement(
      "span",
      {
        className: "fa-layers fa-fw"
      },
      _react["default"].createElement(_reactFontawesome.FontAwesomeIcon, {
        icon: _freeBrandSvgIcons.faBuyNLarge,
        color: "green"
      }),
      _react["default"].createElement(_reactFontawesome.FontAwesomeIcon, {
        size: "3x",
        icon: checkIcon,
        inverse: true,
        transform: "shrink-6"
      })
    )
  );
}
