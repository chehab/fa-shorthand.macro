import React from 'react'
import {
  Icon,
  IconsLayers,
} from 'fa-shorthand.macro'

function App() {
  const checkIcon = Icon.regular.check
  return (
    <div className="app-root">
      <IconsLayers className="foobar-1">
        <Icon icon={Icon.regular.square} color="green"/>
        <Icon icon={checkIcon} inverse transform="shrink-6"/>
      </IconsLayers>

      <IconsLayers>
        <Icon icon={Icon.regularFree.square} color="green"/>
        <Icon size="1x" icon={checkIcon} inverse transform="shrink-6"/>
      </IconsLayers>

      <IconsLayers>
        <Icon icon={Icon.brandFree.buyNLarge} color="green"/>
        <Icon size="3x" icon={checkIcon} inverse transform="shrink-6"/>
      </IconsLayers>
    </div>
  )
}
