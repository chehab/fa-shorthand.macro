## fa-shorthand.macro

[![Npm](https://img.shields.io/npm/v/fa-shorthand.macro?label=npm%20package&logo=logos&style=flat)](https://www.npmjs.com/package/fa-shorthand.macro)
[![Babel Macro](https://img.shields.io/badge/babel--macro-%F0%9F%8E%A3-f5da55.svg?style=flat)](https://github.com/kentcdodds/babel-plugin-macros) 



A [babel-plugin-macros](https://github.com/kentcdodds/babel-plugin-macros) that provide
a shorthand access to [Font Awesome](https://fontawesome.com/) using 
[react-fontawesome](https://www.npmjs.com/package/@fortawesome/react-fontawesome)
 
#### TOC

- [Get started](#get-started)
    - [Peer Dependency](#peer-dependency)
- [Usage](#usagey)
    - [Features](#features)
        - [Layers](#layers)
        - [Stack](#stack)
- [Under The Hood](#under-the-hood)
- [Support](#Support)



## Get Started

This package is for integrating Font Awesome 5 React component using SVG.


### Peer Dependency

Ensure you have [babel-plugin-macros](https://github.com/kentcdodds/babel-plugin-macros) installed within your project.
And the [react-fontawesome](https://www.npmjs.com/package/@fortawesome/react-fontawesome)
installed with the icons packages you are using. 

```bash
$ npm i --save @fortawesome/free-brands-svg-icons
$ npm i --save @fortawesome/free-regular-svg-icons
``` 

If you are a Font Awesome Pro subscriber you can install Pro packages.

```bash
$ npm i --save @fortawesome/pro-solid-svg-icons
$ npm i --save @fortawesome/pro-regular-svg-icons
$ npm i --save @fortawesome/pro-light-svg-icons
$ npm i --save @fortawesome/pro-duotone-svg-icons
``` 
 

## Usage

You can use Font Awesome icons in your React components as simply as this:

```javascript
import { Icon } from 'fa-shorthand.macro'


<Icon icon={Icon.regularFree.check} size="xs"/>
<Icon icon={Icon.brands.buyNLarge}/>
```

```javascript
<Icon icon={Icon.doutone.coffee} size="6x"/>
```

### Features

#### [Layers](https://fontawesome.com/how-to-use/on-the-web/styling/layering)

```javascript
import { Icon, IconsLayers } from 'fa-shorthand.macro'

<IconsLayers>
  <Icon icon={Icon.regularFree.square} color="green" />
  <Icon icon={Icon.regularFree.check} inverse transform="shrink-6" />
</IconsLayers>
```

#### [Stack](https://fontawesome.com/how-to-use/on-the-web/styling/stacking-icons)

A `size` prop is needed for `IconsStack` and `Icon` children. [default 1x] 

`Icon` child with no `size` prop will inherit the `IconsStack` size or default to `1x`

```javascript
import { Icon, IconsStack } from 'fa-shorthand.macro';

<IconsStack size="2x">
  <Icon size="1x" icon={Icon.regularFree.square} color="green" />
  <Icon size="2x" icon={Icon.regularFree.check} inverse transform="shrink-6" />
</IconsStack>
```

#### `<IconsStack />`
| prop | type | default |
|:----:|:----:|:-------:|
|`size`|`string/number`| `1x`|

## Under The Hood

#### Basic 

```javascript
<Icon icon={Icon.regularFree.coffee}/>
```

the code block above will generate the following results 
 
```javascript
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome' // <Icon />
import { faCoffee as farCoffee } from '@fortawesome/free-regular-svg-icons' // Icon.regularFree.coffee

<FontAwesomeIcon icon={farCoffee} />
```

#### Layers

```javascript
<IconsLayers>
  <FontAwesomeIcon icon={Icon.regularFree.square} color="green" />
  <FontAwesomeIcon icon={Icon.regularFree.check} inverse transform="shrink-6" />
</IconsLayers>
```

the code block above will generate the following results 
 
```javascript
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faCoffee as farCheck, faSquare as farSquare } from '@fortawesome/free-regular-svg-icons'


<span className="fa-layers fa-fw">
  <FontAwesomeIcon icon={farSquare} color="green" />
  <FontAwesomeIcon icon={farCheck} inverse transform="shrink-6" />
</span>
```

#### Stack

```javascript
<IconsStack>
  <FontAwesomeIcon icon={Icon.regularFree.square} color="green" />
  <FontAwesomeIcon icon={Icon.regularFree.check} inverse transform="shrink-6" />
</IconsStack>
```

the code block above will generate the following results 
 
```javascript
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faCoffee as farCheck, faSquare as farSquare } from '@fortawesome/free-regular-svg-icons'


<span className="fa-stack fa-stack-1x">
  <FontAwesomeIcon className="fa-stack-1x" icon={farSquare} color="green" />
  <FontAwesomeIcon className="fa-stack-1x" icon={farCheck} inverse transform="shrink-6" />
</span>
```

## Support

Bug/Suggestions open an issue on [Gitlab](https://gitlab.com/chehab/fa-shorthand.macro/issues) or by [email](mailto:incoming+chehab-fa-shorthand-macro-16123939-issue-@incoming.gitlab.com)  
