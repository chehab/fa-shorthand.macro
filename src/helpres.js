const t = require('@babel/types')
const {
  isArray,
  camelCase,
  arrayRemoveAtIndex,
} = require('./utils')


module.exports = {
  getIconVariantMeta,
  getCreateIconIdentifier,
  getIconMetaFromIdentifier,
  jsxAttributeFindByName,
  jsxAttributeRemoveByName,
}


function getCreateIconIdentifier(state) {
  const memo = {}

  return ({ name, variant }) => {
    if (!name && !variant) {
      return { ...memo }
    }

    if (variant.match(/brandsFree/)) {
      variant = 'brands'
    }

    const id = camelCase(`fa-${name}-${variant}`)

    if (memo.hasOwnProperty(id)) {
      return memo[id]
    } else {
      memo[id] = state.file.path.scope.generateUidIdentifier(id)
      return memo[id]
    }
  }
}

/**
 * Find `JSXAttribute` from `JSXOpeningElement` by attribute name
 *
 * @param {JSXOpeningElement} element
 * @param {string} name
 * @param {*} otherwise
 *
 * @return {{ index: number, value: string|number }}
 */
function jsxAttributeFindByName(element, name, otherwise = null) {

  if (!otherwise) {
    otherwise = {
      index: -1,
      value: '',
    }
  }

  if (t.isJSXOpeningElement(element)) {
    const attrs = element.attributes

    if (isArray(attrs)) {
      const ndx = attrs
        .findIndex((a) => (a.name.name === name))

      if (ndx >= 0) {
        let value
        let valueNode
        const node = attrs[ndx]

        if (t.isStringLiteral(node.value)) {
          value = node.value.value
          valueNode = node.value
        } else if (t.isJSXExpressionContainer(node.value) && t.isNumericLiteral(node.value.expression)) {
          value = node.value.expression.value
          valueNode = node.value.expression
        }

        return {
          node,
          value,
          valueNode,
          index: ndx,
        }
      } else {
        return otherwise
      }
    } else {
      return otherwise
    }
  } else {
    return otherwise
  }
}


/**
 * Remove `JSXAttribute` from `JSXOpeningElement` by attribute name
 *
 * @param {JSXOpeningElement} element
 * @param {String} name
 */
function jsxAttributeRemoveByName(element, name) {
  const { index } = jsxAttributeFindByName(element, name)
  arrayRemoveAtIndex(element.attributes, index)
}


/**
 *
 * @param {Identifier} identifier
 */
function getIconMetaFromIdentifier(identifier) {
  if (!(t.isIdentifier(identifier))) {
    return {}
  }

  const pp = identifier.parentPath.parent

  const _variant = pp.object.property.name
  const name = pp.property.name

  const variant = _variant.match(/brands/g)
    ? 'brands'
    : _variant

  return {
    name,
    variant,
  }
}


function getIconVariantMeta(variant) {
  const isBrands = variant === 'brands'
  const isFree = variant.match(/Free$/)

  const _variant = variant.replace(/Free$/, '')

  const flag = (isBrands || isFree)
    ? 'free'
    : 'pro'

  return {
    flag,
    isFree,
    isBrands,
    variant: _variant,
    pkgPath:`@fortawesome/${flag}-${_variant}-svg-icons`
  }
}

