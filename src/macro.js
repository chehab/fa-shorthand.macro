/**
 * ### faProShorthand.macro
 *
 * A [babel-plugin-macros](https://github.com/kentcdodds/babel-plugin-macros) that provide
 * a shorthand access to using [FortAwesome](https://fortawesome.com/) Pro
 *
 */
const t = require('@babel/types')
const {
  entries,
  isArray,
  notArray,
  pascalCase,
} = require('./utils')
const {
  getIconVariantMeta,
  getCreateIconIdentifier,
  getIconMetaFromIdentifier,
  jsxAttributeFindByName,
  jsxAttributeRemoveByName,
} = require('./helpres')



module.exports = function faShorthandMacro(params) {
  const { references, state, babel, ...rest } = params

  params.FaIcon = state.file.path.scope.generateUidIdentifier('FaIcon')

  params.createIconIdentifier = getCreateIconIdentifier(state)

  addIconsImports(params)
  replaceIconsStack(params)
  replaceIconsLayers(params)
  replaceIconsJsx(params)
  replaceIconsFaRef(params)
}


function replaceIconsFaRef({ references, createIconIdentifier }) {
  const { Icon } = references

  if (notArray(Icon)) {
    return null
  }

  Icon.filter(i => t.isIdentifier(i))
    .map((i, ndx) => {
      const {
        name,
        variant,
      } = getIconMetaFromIdentifier(i)

      const iconIdentifier = createIconIdentifier({ variant, name })

      // replace the whole expression
      //    `Icon.{variant}.{name}` => `_Fa{Name}{Variant}`
      i.parentPath.parentPath.replaceWith(iconIdentifier)
    })
}


function replaceIconsJsx({ FaIcon, references, state }) {
  const { Icon } = references

  if (notArray(Icon)) {
    return null
  }

  Icon.filter(i => t.isJSXIdentifier(i))
    .map((i, ndx) => {
      const jsxFaIcon = t.jsxIdentifier(FaIcon.name)

      i.replaceWith(jsxFaIcon)
    })
}


function replaceIconsStack({ FaIcon, references, state }) {
  const { IconsStack } = references

  if (notArray(IconsStack)) {
    return null
  }

  IconsStack.filter(i => t.isJSXOpeningElement(i.parentPath))
    .map((i, ndx) => {
      const pp = i.parentPath.parentPath
      let children = pp.node.children

      const {
        attrs,
        stackSize,
      } = iconsStackWrapperAttributes(i.parent)

      const jsxSpan = t.jsxIdentifier('span')

      const jsxElmOpen = t.jsxOpeningElement(jsxSpan, attrs, false)

      const jsxElmClose = t.jsxClosingElement(jsxSpan)

      if (isArray(children)) {
        children = children
          .map(iconsStackChildAttributesMapper(stackSize))
      }

      const jsxElm = t.jsxElement(jsxElmOpen, jsxElmClose, children, false)

      pp.replaceWith(jsxElm)
    })
}


/**
 * set stack size className based on JSX Attr. `size`
 *  on `IconsStack` size or default to 1x
 *
 * @param {JSXOpeningElement} element
 *
 * @return {{ stackSize: string, attrs: JSXAttribute[] }}
 */
function iconsStackWrapperAttributes(element) {
  let stackSize = '1x'

  // break, if not JSX Opening Element
  if (!t.isJSXOpeningElement(element)) {
    return {
      stackSize,
      attrs: [],
    }
  }

  let attrs = element.attributes

  // get (value, index) of attr. `size`
  const {
    value: sizeAttrValue,
    index: sizeAttrIndex,
  } = jsxAttributeFindByName(element, 'size', { index: -1, value: stackSize })

  // get (value, index) of attr. `className`
  const {
    index: classNameAttrIndex,
    value: classNameAttrValue,
  } = jsxAttributeFindByName(element, 'className')

  // fix size value with post fix if needed
  stackSize = (!!sizeAttrValue.toString()
    .match(/x$/))
    ? sizeAttrValue
    : `${sizeAttrValue}x`

  // remove attrs. `size` and `className`
  jsxAttributeRemoveByName(element, 'size')
  jsxAttributeRemoveByName(element, 'className')

  // create classNameJsxAttr for IconsStack Wrapper
  const classNameAttrValueNew = t.stringLiteral(`fa-stack fa-${stackSize} ${classNameAttrValue}`.trim())
  const classNameAttrs = t.jsxAttribute(t.jsxIdentifier('className'), classNameAttrValueNew)

  // upsert new `className` attr to element
  if (isArray(attrs)) {
    attrs.unshift(classNameAttrs)
  } else {
    attrs = [classNameAttrs]
  }

  return { stackSize, attrs }
}


/**
 * set `Icon` stack size base on JSX Attr `size` on icon
 *  or fallback to IconsStack size or default to 1x
 *
 * @param {String} stackSize
 * @return {function(element: JSXOpeningElement|JSXText) : (JSXOpeningElement|JSXText)}
 */
function iconsStackChildAttributesMapper(stackSize) {
  return (element) => {
    // break, if not JSX Element
    if (!t.isJSXElement(element)) {
      return element
    }

    // get (value, index) of attr. `size`
    const {
      index: sizeIndex,
      value: sizeValue,
    } = jsxAttributeFindByName(element.openingElement, 'size', { index: -1, value: stackSize })

    // get (value, index) of attr. `className`
    const {
      index: classNameIndex,
      value: classNameValue,
    } = jsxAttributeFindByName(element.openingElement, 'className')

    // fix size value with post fix if needed
    const sizePostfix = (!!sizeValue.toString()
      .match(/x$/))
      ? sizeValue
      : `${sizeValue}x`

    // concat. className with stack size
    const classNameValueRaw = `fa-stack-${sizePostfix} ${classNameValue}`.trim()

    const classNameAttrs = t.jsxAttribute(t.jsxIdentifier('className'), t.stringLiteral(classNameValueRaw))

    // remove attrs. `size` and `className`
    jsxAttributeRemoveByName(element.openingElement, 'size')
    jsxAttributeRemoveByName(element.openingElement, 'className')

    // upsert new `className` attr to element
    if (isArray(element.openingElement.attributes)) {
      element.openingElement.attributes.unshift(classNameAttrs)
    } else {
      element.openingElement.attributes = [classNameAttrs]
    }

    return element
  }
}


function replaceIconsLayers({ FaIcon, references, state }) {
  const { IconsLayers } = references

  if (notArray(IconsLayers)) {
    return null
  }

  IconsLayers.filter(i => t.isJSXOpeningElement(i.parentPath))
    .map((i, ndx) => {
      const pp = i.parentPath.parentPath
      const children = pp.node.children
      const attrs = iconsLayersWrapperAttributes(i.parent)

      const jsxSpan = t.jsxIdentifier('span')

      const jsxElmOpen = t.jsxOpeningElement(jsxSpan, attrs, false)

      const jsxElmClose = t.jsxClosingElement(jsxSpan)

      const jsxElm = t.jsxElement(jsxElmOpen, jsxElmClose, children, false)

      pp.replaceWith(jsxElm)
    })
}


/**
 *
 * @param {JSXOpeningElement} jsxOpeningElement
 * @return {Array<JSXAttribute | JSXSpreadAttribute>}
 */
function iconsLayersWrapperAttributes(element) {
  // break, if not JSX Opening Element
  if (!t.isJSXOpeningElement(element)) {
    return {
      stackSize,
      attrs: [],
    }
  }

  let attrs = element.attributes

  // get (value, index) of attr. `className`
  const {
    index: classNameAttrIndex,
    value: classNameAttrValue,
  } = jsxAttributeFindByName(element, 'className')

  // create classNameJsxAttr for IconsStack Wrapper
  const classNameAttrValueNew = t.stringLiteral(`fa-layers fa-fw ${classNameAttrValue}`.trim())
  const classNameAttrs = t.jsxAttribute(t.jsxIdentifier('className'), classNameAttrValueNew)

  // remove attrs. `className`
  jsxAttributeRemoveByName(element, 'className')

  // upsert attrs. with new `className`
  if (isArray(attrs)) {
    attrs.unshift(classNameAttrs)
  } else {
    attrs = [classNameAttrs]
  }

  return attrs
}


function addIconsImports({ references, state, ...rest }) {
  const { Icon } = references

  if (isArray(Icon)) {
    iconsNameImport({ references, state, ...rest })
  }
}


function iconsNameImport({ references, ...rest }) {
  const { Icon } = references

  if (notArray(Icon)) {
    return null
  }

  const { hasJSX, namedIcons } = deduceIconNamedImport({ references, ...rest })

  insertIconNamedImports(namedIcons, { references, ...rest })

  if (hasJSX) {
    insertIconReactImport({ references, ...rest })
  }
}


function deduceIconNamedImport({ references }) {
  const { Icon } = references

  if (notArray(Icon)) {
    return null
  }

  const defaultResult = { hasJSX: false, namedIcons: {} }

  if (notArray(Icon)) {
    return defaultResult
  }

  return Icon.reduce(
    (a, p) => {
      if (t.isJSXIdentifier(p)) {
        a.hasJSX = true
      } else if (t.isIdentifier(p)) {
        const {
          name,
          variant,
        } = getIconMetaFromIdentifier(p)

        try {
          a.namedIcons[variant].add(name)
        } catch (e) {
          a.namedIcons[variant] = new Set([name])
        }
      }

      return a
    },
    defaultResult,
  )
}


function insertIconNamedImports(namedIcons, { state, createIconIdentifier }) {
  entries(namedIcons)
    .reduce((a, [variant, names], ndx, { length: len }) => {
      a[variant] = Array.from(names)
        .map(name => {
          const localIdentifier = createIconIdentifier({ variant, name })

          const importedIdentifier = t.identifier(`fa${pascalCase(name)}`)

          return t.importSpecifier(localIdentifier, importedIdentifier)
        })

      if (ndx === (len - 1)) {
        return entries(a)
      }

      return a
    }, {})
    .forEach(([variant, namedSpecifier]) => {
      const { pkgPath } = getIconVariantMeta(variant)

      const importPath = t.stringLiteral(pkgPath)

      const importDeclaration = t.importDeclaration(namedSpecifier, importPath)

      state.file.path.unshiftContainer('body', importDeclaration)
    })
}


function insertIconReactImport({ FaIcon, state }) {
  const localIdentifier = FaIcon
  const importIdentifier = t.identifier('FontAwesomeIcon')
  const importPath = t.stringLiteral('@fortawesome/react-fontawesome')
  const importDefaultSpecifier = t.importSpecifier(localIdentifier, importIdentifier)
  const importDeclaration = t.importDeclaration([importDefaultSpecifier], importPath)
  state.file.path.unshiftContainer('body', importDeclaration)
}
