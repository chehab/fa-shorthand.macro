"use strict";

var _require = require('babel-plugin-macros'),
    createMacro = _require.createMacro;

module.exports = createMacro(require('./macro'));