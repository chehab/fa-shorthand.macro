"use strict";

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance"); }

function _iterableToArrayLimit(arr, i) { if (!(Symbol.iterator in Object(arr) || Object.prototype.toString.call(arr) === "[object Arguments]")) { return; } var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

/**
 * ### faProShorthand.macro
 *
 * A [babel-plugin-macros](https://github.com/kentcdodds/babel-plugin-macros) that provide
 * a shorthand access to using [FortAwesome](https://fortawesome.com/) Pro
 *
 */
var t = require('@babel/types');

var _require = require('./utils'),
    entries = _require.entries,
    isArray = _require.isArray,
    notArray = _require.notArray,
    pascalCase = _require.pascalCase;

var _require2 = require('./helpres'),
    getIconVariantMeta = _require2.getIconVariantMeta,
    getCreateIconIdentifier = _require2.getCreateIconIdentifier,
    getIconMetaFromIdentifier = _require2.getIconMetaFromIdentifier,
    jsxAttributeFindByName = _require2.jsxAttributeFindByName,
    jsxAttributeRemoveByName = _require2.jsxAttributeRemoveByName;

module.exports = function faShorthandMacro(params) {
  var references = params.references,
      state = params.state,
      babel = params.babel,
      rest = _objectWithoutProperties(params, ["references", "state", "babel"]);

  params.FaIcon = state.file.path.scope.generateUidIdentifier('FaIcon');
  params.createIconIdentifier = getCreateIconIdentifier(state);
  addIconsImports(params);
  replaceIconsStack(params);
  replaceIconsLayers(params);
  replaceIconsJsx(params);
  replaceIconsFaRef(params);
};

function replaceIconsFaRef(_ref) {
  var references = _ref.references,
      createIconIdentifier = _ref.createIconIdentifier;
  var Icon = references.Icon;

  if (notArray(Icon)) {
    return null;
  }

  Icon.filter(function (i) {
    return t.isIdentifier(i);
  }).map(function (i, ndx) {
    var _getIconMetaFromIdent = getIconMetaFromIdentifier(i),
        name = _getIconMetaFromIdent.name,
        variant = _getIconMetaFromIdent.variant;

    var iconIdentifier = createIconIdentifier({
      variant: variant,
      name: name
    }); // replace the whole expression
    //    `Icon.{variant}.{name}` => `_Fa{Name}{Variant}`

    i.parentPath.parentPath.replaceWith(iconIdentifier);
  });
}

function replaceIconsJsx(_ref2) {
  var FaIcon = _ref2.FaIcon,
      references = _ref2.references,
      state = _ref2.state;
  var Icon = references.Icon;

  if (notArray(Icon)) {
    return null;
  }

  Icon.filter(function (i) {
    return t.isJSXIdentifier(i);
  }).map(function (i, ndx) {
    var jsxFaIcon = t.jsxIdentifier(FaIcon.name);
    i.replaceWith(jsxFaIcon);
  });
}

function replaceIconsStack(_ref3) {
  var FaIcon = _ref3.FaIcon,
      references = _ref3.references,
      state = _ref3.state;
  var IconsStack = references.IconsStack;

  if (notArray(IconsStack)) {
    return null;
  }

  IconsStack.filter(function (i) {
    return t.isJSXOpeningElement(i.parentPath);
  }).map(function (i, ndx) {
    var pp = i.parentPath.parentPath;
    var children = pp.node.children;

    var _iconsStackWrapperAtt = iconsStackWrapperAttributes(i.parent),
        attrs = _iconsStackWrapperAtt.attrs,
        stackSize = _iconsStackWrapperAtt.stackSize;

    var jsxSpan = t.jsxIdentifier('span');
    var jsxElmOpen = t.jsxOpeningElement(jsxSpan, attrs, false);
    var jsxElmClose = t.jsxClosingElement(jsxSpan);

    if (isArray(children)) {
      children = children.map(iconsStackChildAttributesMapper(stackSize));
    }

    var jsxElm = t.jsxElement(jsxElmOpen, jsxElmClose, children, false);
    pp.replaceWith(jsxElm);
  });
}
/**
 * set stack size className based on JSX Attr. `size`
 *  on `IconsStack` size or default to 1x
 *
 * @param {JSXOpeningElement} element
 *
 * @return {{ stackSize: string, attrs: JSXAttribute[] }}
 */


function iconsStackWrapperAttributes(element) {
  var stackSize = '1x'; // break, if not JSX Opening Element

  if (!t.isJSXOpeningElement(element)) {
    return {
      stackSize: stackSize,
      attrs: []
    };
  }

  var attrs = element.attributes; // get (value, index) of attr. `size`

  var _jsxAttributeFindByNa = jsxAttributeFindByName(element, 'size', {
    index: -1,
    value: stackSize
  }),
      sizeAttrValue = _jsxAttributeFindByNa.value,
      sizeAttrIndex = _jsxAttributeFindByNa.index; // get (value, index) of attr. `className`


  var _jsxAttributeFindByNa2 = jsxAttributeFindByName(element, 'className'),
      classNameAttrIndex = _jsxAttributeFindByNa2.index,
      classNameAttrValue = _jsxAttributeFindByNa2.value; // fix size value with post fix if needed


  stackSize = !!sizeAttrValue.toString().match(/x$/) ? sizeAttrValue : "".concat(sizeAttrValue, "x"); // remove attrs. `size` and `className`

  jsxAttributeRemoveByName(element, 'size');
  jsxAttributeRemoveByName(element, 'className'); // create classNameJsxAttr for IconsStack Wrapper

  var classNameAttrValueNew = t.stringLiteral("fa-stack fa-".concat(stackSize, " ").concat(classNameAttrValue).trim());
  var classNameAttrs = t.jsxAttribute(t.jsxIdentifier('className'), classNameAttrValueNew); // upsert new `className` attr to element

  if (isArray(attrs)) {
    attrs.unshift(classNameAttrs);
  } else {
    attrs = [classNameAttrs];
  }

  return {
    stackSize: stackSize,
    attrs: attrs
  };
}
/**
 * set `Icon` stack size base on JSX Attr `size` on icon
 *  or fallback to IconsStack size or default to 1x
 *
 * @param {String} stackSize
 * @return {function(element: JSXOpeningElement|JSXText) : (JSXOpeningElement|JSXText)}
 */


function iconsStackChildAttributesMapper(stackSize) {
  return function (element) {
    // break, if not JSX Element
    if (!t.isJSXElement(element)) {
      return element;
    } // get (value, index) of attr. `size`


    var _jsxAttributeFindByNa3 = jsxAttributeFindByName(element.openingElement, 'size', {
      index: -1,
      value: stackSize
    }),
        sizeIndex = _jsxAttributeFindByNa3.index,
        sizeValue = _jsxAttributeFindByNa3.value; // get (value, index) of attr. `className`


    var _jsxAttributeFindByNa4 = jsxAttributeFindByName(element.openingElement, 'className'),
        classNameIndex = _jsxAttributeFindByNa4.index,
        classNameValue = _jsxAttributeFindByNa4.value; // fix size value with post fix if needed


    var sizePostfix = !!sizeValue.toString().match(/x$/) ? sizeValue : "".concat(sizeValue, "x"); // concat. className with stack size

    var classNameValueRaw = "fa-stack-".concat(sizePostfix, " ").concat(classNameValue).trim();
    var classNameAttrs = t.jsxAttribute(t.jsxIdentifier('className'), t.stringLiteral(classNameValueRaw)); // remove attrs. `size` and `className`

    jsxAttributeRemoveByName(element.openingElement, 'size');
    jsxAttributeRemoveByName(element.openingElement, 'className'); // upsert new `className` attr to element

    if (isArray(element.openingElement.attributes)) {
      element.openingElement.attributes.unshift(classNameAttrs);
    } else {
      element.openingElement.attributes = [classNameAttrs];
    }

    return element;
  };
}

function replaceIconsLayers(_ref4) {
  var FaIcon = _ref4.FaIcon,
      references = _ref4.references,
      state = _ref4.state;
  var IconsLayers = references.IconsLayers;

  if (notArray(IconsLayers)) {
    return null;
  }

  IconsLayers.filter(function (i) {
    return t.isJSXOpeningElement(i.parentPath);
  }).map(function (i, ndx) {
    var pp = i.parentPath.parentPath;
    var children = pp.node.children;
    var attrs = iconsLayersWrapperAttributes(i.parent);
    var jsxSpan = t.jsxIdentifier('span');
    var jsxElmOpen = t.jsxOpeningElement(jsxSpan, attrs, false);
    var jsxElmClose = t.jsxClosingElement(jsxSpan);
    var jsxElm = t.jsxElement(jsxElmOpen, jsxElmClose, children, false);
    pp.replaceWith(jsxElm);
  });
}
/**
 *
 * @param {JSXOpeningElement} jsxOpeningElement
 * @return {Array<JSXAttribute | JSXSpreadAttribute>}
 */


function iconsLayersWrapperAttributes(element) {
  // break, if not JSX Opening Element
  if (!t.isJSXOpeningElement(element)) {
    return {
      stackSize: stackSize,
      attrs: []
    };
  }

  var attrs = element.attributes; // get (value, index) of attr. `className`

  var _jsxAttributeFindByNa5 = jsxAttributeFindByName(element, 'className'),
      classNameAttrIndex = _jsxAttributeFindByNa5.index,
      classNameAttrValue = _jsxAttributeFindByNa5.value; // create classNameJsxAttr for IconsStack Wrapper


  var classNameAttrValueNew = t.stringLiteral("fa-layers fa-fw ".concat(classNameAttrValue).trim());
  var classNameAttrs = t.jsxAttribute(t.jsxIdentifier('className'), classNameAttrValueNew); // remove attrs. `className`

  jsxAttributeRemoveByName(element, 'className'); // upsert attrs. with new `className`

  if (isArray(attrs)) {
    attrs.unshift(classNameAttrs);
  } else {
    attrs = [classNameAttrs];
  }

  return attrs;
}

function addIconsImports(_ref5) {
  var references = _ref5.references,
      state = _ref5.state,
      rest = _objectWithoutProperties(_ref5, ["references", "state"]);

  var Icon = references.Icon;

  if (isArray(Icon)) {
    iconsNameImport(_objectSpread({
      references: references,
      state: state
    }, rest));
  }
}

function iconsNameImport(_ref6) {
  var references = _ref6.references,
      rest = _objectWithoutProperties(_ref6, ["references"]);

  var Icon = references.Icon;

  if (notArray(Icon)) {
    return null;
  }

  var _deduceIconNamedImpor = deduceIconNamedImport(_objectSpread({
    references: references
  }, rest)),
      hasJSX = _deduceIconNamedImpor.hasJSX,
      namedIcons = _deduceIconNamedImpor.namedIcons;

  insertIconNamedImports(namedIcons, _objectSpread({
    references: references
  }, rest));

  if (hasJSX) {
    insertIconReactImport(_objectSpread({
      references: references
    }, rest));
  }
}

function deduceIconNamedImport(_ref7) {
  var references = _ref7.references;
  var Icon = references.Icon;

  if (notArray(Icon)) {
    return null;
  }

  var defaultResult = {
    hasJSX: false,
    namedIcons: {}
  };

  if (notArray(Icon)) {
    return defaultResult;
  }

  return Icon.reduce(function (a, p) {
    if (t.isJSXIdentifier(p)) {
      a.hasJSX = true;
    } else if (t.isIdentifier(p)) {
      var _getIconMetaFromIdent2 = getIconMetaFromIdentifier(p),
          name = _getIconMetaFromIdent2.name,
          variant = _getIconMetaFromIdent2.variant;

      try {
        a.namedIcons[variant].add(name);
      } catch (e) {
        a.namedIcons[variant] = new Set([name]);
      }
    }

    return a;
  }, defaultResult);
}

function insertIconNamedImports(namedIcons, _ref8) {
  var state = _ref8.state,
      createIconIdentifier = _ref8.createIconIdentifier;
  entries(namedIcons).reduce(function (a, _ref9, ndx, _ref10) {
    var _ref11 = _slicedToArray(_ref9, 2),
        variant = _ref11[0],
        names = _ref11[1];

    var len = _ref10.length;
    a[variant] = Array.from(names).map(function (name) {
      var localIdentifier = createIconIdentifier({
        variant: variant,
        name: name
      });
      var importedIdentifier = t.identifier("fa".concat(pascalCase(name)));
      return t.importSpecifier(localIdentifier, importedIdentifier);
    });

    if (ndx === len - 1) {
      return entries(a);
    }

    return a;
  }, {}).forEach(function (_ref12) {
    var _ref13 = _slicedToArray(_ref12, 2),
        variant = _ref13[0],
        namedSpecifier = _ref13[1];

    var _getIconVariantMeta = getIconVariantMeta(variant),
        pkgPath = _getIconVariantMeta.pkgPath;

    var importPath = t.stringLiteral(pkgPath);
    var importDeclaration = t.importDeclaration(namedSpecifier, importPath);
    state.file.path.unshiftContainer('body', importDeclaration);
  });
}

function insertIconReactImport(_ref14) {
  var FaIcon = _ref14.FaIcon,
      state = _ref14.state;
  var localIdentifier = FaIcon;
  var importIdentifier = t.identifier('FontAwesomeIcon');
  var importPath = t.stringLiteral('@fortawesome/react-fontawesome');
  var importDefaultSpecifier = t.importSpecifier(localIdentifier, importIdentifier);
  var importDeclaration = t.importDeclaration([importDefaultSpecifier], importPath);
  state.file.path.unshiftContainer('body', importDeclaration);
}