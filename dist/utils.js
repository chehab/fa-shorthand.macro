"use strict";

var entries = Object.entries;
var isArray = Array.isArray;

var notArray = function notArray() {
  return !isArray.apply(void 0, arguments);
};

module.exports = {
  entries: entries,
  isArray: isArray,
  notArray: notArray,
  camelCase: camelCase,
  pascalCase: pascalCase,
  capitalize: capitalize,
  arrayRemoveAtIndex: arrayRemoveAtIndex
};
/**
 * convert string case to `capitalize`
 *
 * @example
 *  foo-bar => Foo-Bar
 *
 * @param {string} phrase
 * @return {string}
 */

function capitalize(phrase) {
  return phrase.split('').reduce(function (a, c) {
    if (a.isPostWhitespace) {
      a.capPhrase += c.toUpperCase();
    } else {
      a.capPhrase += c;
    }

    a.isPostWhitespace = !!c.match(/[\s-_\.]/);
    return a;
  }, {
    isPostWhitespace: true,
    capPhrase: ''
  });
}
/**
 * convert string case to `camelCase`
 *
 * @example
 *  foo-bar => fooBar
 *
 * @param {string} phrase
 * @return {string}
 */


function camelCase(phrase) {
  return phrase.split('').reduce(function (a, c) {
    var isWhitespace = !!c.match(/[\s-_\.]/);
    var isDigit = !!c.match(/\d/);

    if (!isWhitespace && a.isPostWhitespace) {
      a.capPhrase += c.toUpperCase();
    } else if (!isWhitespace) {
      a.capPhrase += c;
    }

    a.isPostWhitespace = isWhitespace || isDigit;
    return a;
  }, {
    isPostWhitespace: false,
    capPhrase: ''
  }).capPhrase;
}
/**
 * convert string case to `pascalCase`
 *
 * @example
 *  foo-bar => FooBar
 *
 * @param {string} phrase
 * @return {string}
 */


function pascalCase(phrase) {
  var p = camelCase(phrase);

  if (p.length) {
    return p.replace(/^./, p[0].toUpperCase());
  }

  return p;
}
/**
 * Remove item for an array at index
 *
 * @param {Array} array
 * @param {number} index
 */


function arrayRemoveAtIndex(array, index) {
  if (index !== -1) {
    array.splice(index, 1);
  }
}