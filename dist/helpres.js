"use strict";

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var t = require('@babel/types');

var _require = require('./utils'),
    isArray = _require.isArray,
    camelCase = _require.camelCase,
    arrayRemoveAtIndex = _require.arrayRemoveAtIndex;

module.exports = {
  getIconVariantMeta: getIconVariantMeta,
  getCreateIconIdentifier: getCreateIconIdentifier,
  getIconMetaFromIdentifier: getIconMetaFromIdentifier,
  jsxAttributeFindByName: jsxAttributeFindByName,
  jsxAttributeRemoveByName: jsxAttributeRemoveByName
};

function getCreateIconIdentifier(state) {
  var memo = {};
  return function (_ref) {
    var name = _ref.name,
        variant = _ref.variant;

    if (!name && !variant) {
      return _objectSpread({}, memo);
    }

    if (variant.match(/brandsFree/)) {
      variant = 'brands';
    }

    var id = camelCase("fa-".concat(name, "-").concat(variant));

    if (memo.hasOwnProperty(id)) {
      return memo[id];
    } else {
      memo[id] = state.file.path.scope.generateUidIdentifier(id);
      return memo[id];
    }
  };
}
/**
 * Find `JSXAttribute` from `JSXOpeningElement` by attribute name
 *
 * @param {JSXOpeningElement} element
 * @param {string} name
 * @param {*} otherwise
 *
 * @return {{ index: number, value: string|number }}
 */


function jsxAttributeFindByName(element, name) {
  var otherwise = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : null;

  if (!otherwise) {
    otherwise = {
      index: -1,
      value: ''
    };
  }

  if (t.isJSXOpeningElement(element)) {
    var attrs = element.attributes;

    if (isArray(attrs)) {
      var ndx = attrs.findIndex(function (a) {
        return a.name.name === name;
      });

      if (ndx >= 0) {
        var value;
        var valueNode;
        var node = attrs[ndx];

        if (t.isStringLiteral(node.value)) {
          value = node.value.value;
          valueNode = node.value;
        } else if (t.isJSXExpressionContainer(node.value) && t.isNumericLiteral(node.value.expression)) {
          value = node.value.expression.value;
          valueNode = node.value.expression;
        }

        return {
          node: node,
          value: value,
          valueNode: valueNode,
          index: ndx
        };
      } else {
        return otherwise;
      }
    } else {
      return otherwise;
    }
  } else {
    return otherwise;
  }
}
/**
 * Remove `JSXAttribute` from `JSXOpeningElement` by attribute name
 *
 * @param {JSXOpeningElement} element
 * @param {String} name
 */


function jsxAttributeRemoveByName(element, name) {
  var _jsxAttributeFindByNa = jsxAttributeFindByName(element, name),
      index = _jsxAttributeFindByNa.index;

  arrayRemoveAtIndex(element.attributes, index);
}
/**
 *
 * @param {Identifier} identifier
 */


function getIconMetaFromIdentifier(identifier) {
  if (!t.isIdentifier(identifier)) {
    return {};
  }

  var pp = identifier.parentPath.parent;
  var _variant = pp.object.property.name;
  var name = pp.property.name;
  var variant = _variant.match(/brands/g) ? 'brands' : _variant;
  return {
    name: name,
    variant: variant
  };
}

function getIconVariantMeta(variant) {
  var isBrands = variant === 'brands';
  var isFree = variant.match(/Free$/);

  var _variant = variant.replace(/Free$/, '');

  var flag = isBrands || isFree ? 'free' : 'pro';
  return {
    flag: flag,
    isFree: isFree,
    isBrands: isBrands,
    variant: _variant,
    pkgPath: "@fortawesome/".concat(flag, "-").concat(_variant, "-svg-icons")
  };
}